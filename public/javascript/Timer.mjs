export var Timer = function (obj){
    this.time = obj.time
    this.onTick = obj.onTick || null
    this.start = () =>{
        this.interval = setInterval(this.update,1000)
    }
    this.stop = () =>{
        clearInterval(this.interval)
    }
    this.update = () =>{
        this.time > 0? this.time -=1:this.stop()
        this.onTick? this.onTick():void 0
        return this.get()
    }
    this.get = () => {
        return this.time
    }
}

