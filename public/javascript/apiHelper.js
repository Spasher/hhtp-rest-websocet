
export async function callApi(endpoint, method) {
    const options = {
        method,
    };
    return await fetch(endpoint, options)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            return data;
        });
}