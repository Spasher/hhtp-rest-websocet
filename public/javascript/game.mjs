import {createElement,addClass, removeClass, formatClassNames} from "./helper.mjs";
import {callApi} from "./apiHelper.js";
import {Timer} from "./Timer.mjs";

const username = sessionStorage.getItem("username");

let activeRoomId = null;

const setActiveRoomId = roomId => {
  activeRoomId = roomId;
};

if (!username) {
  window.location.replace("/login");
}
const gamePage = document.getElementById('game-page')
const roomItems = document.getElementById('rooms')
const roomPage = document.getElementById('rooms-page')

const addRoomBtn = document.getElementById('add-room-btn')
const quitBtn = document.getElementById('quit-room-btn')


addRoomBtn.addEventListener('click', addRoom)
quitBtn.addEventListener('click',quiteRoom)

function quiteRoom(){
  addClass(gamePage, "display-none");
  removeClass(roomPage,'display-none')
  socket.emit("LEAVE_ROOM",socket)
}

function addRoom(){
  const roomName = prompt('Enter Room Name')
  socket.emit('CREATE_ROOM', roomName)
}

const createRoomButton = roomId => {
  const roomBlock = createElement({
    tagName: "div",
    className: "room",
    attributes: { id: roomId }
  });
  const roomButton = createElement({
    tagName: "button",
    className: "join-btn"
  })
  const participantsCount = createElement({
    tagName:'div',
    id:'part-block'
  })


  const onJoinRoom = () => {
    socket.emit("JOIN_ROOM", roomId);
  };

  roomButton.addEventListener("click", onJoinRoom);
  roomButton.innerText = 'JOIN'
  roomBlock.innerText = roomId;
  roomBlock.appendChild(roomButton)
  roomBlock.appendChild(participantsCount)
  return roomBlock;
};

const showGameWindow = users =>{
  const usersBlock = document.getElementById('userBlock')
  const allUsers = users.map(createUserBlock)
  const usersList = usersBlock.children
  const readyBtn = createElement({
    tagName:'button',
    className:'ready-btn'
  })
  function onReady(){
    for (let i =0;i<usersList.length;i++){
      if (usersList[i].id === socket.query.username){
        socket.emit('GET_READY',socket.query.username)
        usersBlock.children[i].firstChild.style.color = 'green'
      }
    }

  }
  readyBtn.innerText = 'READY'
  readyBtn.addEventListener('click',onReady)
  usersBlock.innerHTML = ''
  usersBlock.append(...allUsers)
  usersBlock.append(readyBtn)
  gamePage.appendChild(usersBlock)


}
const createUserBlock = userId => {
  const userBlock = createElement({
    tagName: "div",
    className: "user-block",
    attributes: {id: userId}
  });
  userBlock.innerHTML = `<div class=userName>${userId}</div>`
  const userBar = createElement({
    tagName:'div',
    className:'user-bar'
  })
  userBlock.appendChild(userBar)
  return userBlock
}
const startGame = async users =>{
  const qiutBtn = document.getElementById('quit-room-btn')
  let textGame = await callApi('http://localhost:3006/game/texts/1','GET')
  const timer1 = new Timer({
    time:10,
    onTick:tick
  })
  timer1.start()
  addClass(quitBtn,'display-none')
  const gameWindow = createElement({
    tagName:'div',
    className:'game-window'

  })
  function tick(){
    gameWindow.innerText = timer1.get()
    if(timer1.get() === 0){
      gameWindow.innerText = textGame.textGame
      socket.emit('PLAY_GAME', textGame.textGame)
    }
  }
  requestAnimationFrame(tick)
  gamePage.appendChild(gameWindow)
}

const updateShowRooms = rooms => {

  const allRooms = rooms.map(createRoomButton)

  roomItems.innerHTML = ""
  roomItems.append(...allRooms)
}
const checkInput = letters =>{
  let counter = 0
  const onKeyUp = ev => {
    if (ev.key === letters[counter]) {
      counter += 1
      if(counter === letters.length){
        gamePage.lastChild.style.color = 'green'
      }
    }
  };

  window.addEventListener('keyup', onKeyUp)
}
const updateCounter = (count=0,roomId) => {
  const counter = document.getElementById('rooms')
  const rooms = counter.children

  for (let i=0; i<rooms.length;i++){
    if(rooms[i].id === roomId){
      counter.children[i].lastChild.innerText = `${count}participants`
    }
  }
}

const joinRoomDone = roomId => {
  const newRoomElement = document.getElementById(roomId);
  addClass(newRoomElement, "active");

  if (activeRoomId) {

    const previousRoomElement = document.getElementById(activeRoomId);
    removeClass(previousRoomElement, "active");
  }

  addClass(roomPage, "display-none");
  setActiveRoomId(roomId);
  removeClass(gamePage,'display-none')
};

const socket = io("", { query: { username } })

socket.on('CHECK_INPUT',checkInput)
socket.on('START_GAME',startGame)
socket.on('SHOW_GAME_WINDOW',showGameWindow)
socket.on('UPDATE_SHOW_ROOMS',updateShowRooms)
socket.on("JOIN_ROOM_DONE", joinRoomDone)
socket.on('UPDATE_COUNTER', updateCounter)



