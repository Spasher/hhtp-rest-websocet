
export default io => {
    io.on('connection', socket => {
        socket.emit('UPDATE_ROOMS', rooms)
        socket.on('CREATE_ROOM', roomName =>{
            rooms.push(roomName)

        })

        socket.on('JOIN_ROOM', roomId => {
            const prevRoomId = getCurrentRoomId(socket)
            if(roomId === prevRoomId){
                return
            }
            if(prevRoomId){
                socket.leave(prevRoomId)
            }
            socket.join(roomId,()=>{
                const counterValue = counterMap.get(roomId)
                io.to(socket.id).emit("JOIN_ROOM_DONE",{counterValue,roomId})
            })

        })
    })
}