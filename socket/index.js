import * as config from "./config";
import {addUser, deleteUser} from "../user/users";


const winners = []
const rooms = ['Room1','Room2','Room3']
const counterMap = new Map(rooms.map(roomId => [roomId,[]]))
let getReady = 0
const getCurrentRoomId = socket => Object.keys(socket.rooms).find(roomId => counterMap.has(roomId))



export default io => {

  io.sockets.on("connection", socket => {

    const username = socket.handshake.query.username;
    console.log(`${username} connected`)

    socket.emit('UPDATE_SHOW_ROOMS',rooms)
    io.emit('UPDATE_COUNTER')

    socket.on('CREATE_ROOM', roomName =>{
      rooms.push(roomName)
      socket.emit('UPDATE_SHOW_ROOMS',rooms)
    })

    socket.on('JOIN_ROOM', roomId =>{

      const users = counterMap.get(roomId)
      const prevRoomId = getCurrentRoomId(socket)

      if(roomId === prevRoomId){
        return
      }
      if (prevRoomId){
        socket.leave(prevRoomId)
        const prevRoomPart = counterMap.get(prevRoomId)
        prevRoomPart.splice(prevRoomPart.indexOf(roomId),1)
        io.emit('UPDATE_COUNTER',prevRoomPart.length,prevRoomId)
      }
      users.push(username)
      counterMap.set(roomId,users)

      socket.on('GET_READY', userid=>{
        getReady += 1
        if(getReady === users.length){
          console.log('start')
          io.emit('START_GAME',users)
        }
      })
      socket.join(roomId,()=>{

        io.to(socket.id).emit('JOIN_ROOM_DONE', roomId)
        io.emit('UPDATE_COUNTER',users.length,roomId)
        io.emit('SHOW_GAME_WINDOW',users)

      })
    socket.on('PLAY_GAME', text => {
      const letterlist = text.split('')
      socket.emit('CHECK_INPUT', letterlist)
    })
    socket.on('FINIST_INPUT', socket =>{
      winners.push(username)
      console.log('winners')
      console.log(winners)

    })
    socket.on('LEAVE_ROOM',socket =>{
     //error Maximum call stack size exceeded
       })
    })

    socket.on("disconnect",()=>{
      console.log(`${username} disconnected`)
    })
  });
};